import { getSubmissions } from '$lib/submissions';

export const load = async () => {
	const submissions = await getSubmissions();

	return { submissions };
};
