import { startOfToday, format, subDays } from 'date-fns';
import parse from 'html-dom-parser';

const filterWord = 'critme';
const cacheLifespan = 10 * 60 * 1000;

const formatDadDate = (date: Date) => format(date, 'yyyy-MM-dd');

const titleFilter = (title: string) => title.toLowerCase().includes(filterWord);

const getSubmissionsElements = (element) => {
	if (element.type !== 'tag') {
		return [];
	}

	if (element.name === 'div' && element.attribs['class'] === 'submissionIcon') {
		return [element];
	}

	return element.children.flatMap((el) => getSubmissionsElements(el));
};

const parseCount = (str: string) => (parseInt(str) ? parseInt(str) : 0);

const extractData = (element) => {
	const authorElement = element.children[0];
	const authorName = authorElement.children[1]?.data;
	const submissionThumbnailElement = element.children[1];
	const title = submissionThumbnailElement.children[0].attribs['alt'];
	const image = submissionThumbnailElement.children[0].attribs['src'];
	const url = 'https://dad.gallery' + submissionThumbnailElement.attribs['href'];
	const submissionsNotesElement = element.children[2];

	let commentsCount = 0;

	if (submissionsNotesElement.children.length === 3) {
		const { data } = submissionsNotesElement.children[2];

		commentsCount = parseCount(data);
	}

	if (submissionsNotesElement.children.length === 5) {
		const { data } = submissionsNotesElement.children[4];

		commentsCount = parseCount(data);
	}

	return { authorName, title, url, image, commentsCount };
};

const fetchHtml = async (url: string) => {
	const submissionsPage = await fetch(url);
	const text = await submissionsPage.text();

	return parse(text)[1];
};

const fetchPage = async (url: string) => {
	const html = await fetchHtml(url);
	const elements = getSubmissionsElements(html);

	return elements.map((e) => extractData(e)).filter((s) => titleFilter(s.title));
};

const cache = {
	lastUpdate: 0,
	today: [] as any[],
	old: [] as any[]
};

const getPageSubmissions = (date: Date | number) =>
	fetchPage(`https://dad.gallery/submissions?date=${formatDadDate(date)}`);

export const getSubmissions = async () => {
	const { lastUpdate, today, old } = cache;

	if (Date.now() - lastUpdate < cacheLifespan) {
		return { today, old };
	}

	const [updatedToday, day1, day2, day3] = await Promise.all([
		await getPageSubmissions(startOfToday()),
		await getPageSubmissions(subDays(startOfToday(), 1)),
		await getPageSubmissions(subDays(startOfToday(), 2)),
		await getPageSubmissions(subDays(startOfToday(), 3))
	]);

	const updatedOld = [...day1, ...day2, ...day3];

	cache.today = updatedToday;
	cache.old = updatedOld;
	cache.lastUpdate = Date.now();

	return { today: cache.today, old: cache.old };
};
