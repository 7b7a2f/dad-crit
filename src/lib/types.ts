export interface Submission {
	authorName: string;
	title: string;
	url: string;
	image: string;
}
